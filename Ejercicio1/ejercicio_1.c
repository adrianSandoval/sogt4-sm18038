#include <stdio.h>

int main() {
    float n1;
    float n2;
    float *p1;
    float *p2;

    n1 = 4.0;
    p1 = &n1;
    p2 = p1;
    n2 = *p2;
    n1 = *p1 + *p2;

    printf("n1 = %.1f\n", n1);
    printf("n2 = %.1f\n", n2);

    return 0;
}
