#include <stdio.h>
#include <string.h>

typedef struct {
    char nombre[40];
    int dui;
    float sueldo;
} empleado;

int main() {
    empleado empleados[1000];
    int num_empleados = 0;
    float sueldos = 0.0;

    while (1) {
        if (num_empleados >= 1000) {
            printf("Máximo número de empleados alcanzado (1000).\n");
            break;
        }

        printf("Ingrese el nombre del empleado #%d (o 'fin' para terminar): ", num_empleados + 1);
        scanf("%s", empleados[num_empleados].nombre);

        if (strcmp(empleados[num_empleados].nombre, "fin") == 0) {
            break;
        }

        printf("Ingrese el DUI del empleado #%d: ", num_empleados + 1);
        scanf("%d", &empleados[num_empleados].dui);

        printf("Ingrese el sueldo del empleado #%d: ", num_empleados + 1);
        scanf("%f", &empleados[num_empleados].sueldo);

        sueldos += empleados[num_empleados].sueldo;
        num_empleados++;
    }

    // Ordenar el array por el  campo nombre
    for (int i = 0; i < num_empleados - 1; i++) {
        for (int j = 0; j < num_empleados - i - 1; j++) {
            if (strcmp(empleados[j].nombre, empleados[j + 1].nombre) > 0) {
                empleado temp = empleados[j];
                empleados[j] = empleados[j + 1];
                empleados[j + 1] = temp;
            }
        }
    }

    // Empleados ordenados por nombre
    printf("\nEmpleados ordenados por nombre:\n");
    for (int i = 0; i < num_empleados; i++) {
        printf("Nombre: %s, DUI: %d, Sueldo: %.2f\n", empleados[i].nombre, empleados[i].dui, empleados[i].sueldo);
    }

    // Ordenar el array por el campo sueldo
    for (int i = 0; i < num_empleados - 1; i++) {
        for (int j = 0; j < num_empleados - i - 1; j++) {
            if (empleados[j].sueldo < empleados[j + 1].sueldo) {
                empleado temp = empleados[j];
                empleados[j] = empleados[j + 1];
                empleados[j + 1] = temp;
            }
        }
    }

    // Empleados ordenados por sueldo mayor a menor
    printf("\nEmpleados ordenados por sueldo (mayor a menor):\n");
    for (int i = 0; i < num_empleados; i++) {
        printf("Nombre: %s, DUI: %d, Sueldo: %.2f\n", empleados[i].nombre, empleados[i].dui, empleados[i].sueldo);
    }

    // Promedio de los sueldos
    if (num_empleados > 0) {
        float promedio_sueldos = sueldos / num_empleados;
        printf("\nPromedio de sueldos: %.2f\n", promedio_sueldos);
    } else {
        printf("\nNo se ingresaron empleados, no es posible calcular el promedio de sueldos.\n");
    }

    return 0;
}
