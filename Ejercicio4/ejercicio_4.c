#include <stdlib.h>
#include <stdio.h>

int main() {
    int *pnumero;
    int num1, num2;
    char *pchar;
    char letra1;

    num1 = 2;
    num2 = 5;
    letra1 = 'a';
    
    pnumero = &num1;

  // Imprimir la dirección de memoria apuntada por pnumero y el valor de num1
  //printf("Dirección de memoria apuntada por pnumero: %p\n", (void *)pnumero);
    printf("Valor de pnumero: %d\n", *pnumero);


    return 0;
}
