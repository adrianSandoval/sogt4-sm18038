#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NUM_VALORES 10 

int ordenarDeMayorAMenor(const void *a, const void *b) {
    return (*(int *)b - *(int *)a);
}

int main() {
    int *valores = (int *)malloc(NUM_VALORES * sizeof(int));

    // Funcion srand para inicializar la generación de números pseudoaleatorios
    srand(time(NULL));
    for (int i = 0; i < NUM_VALORES; i++) {
        valores[i] = rand() % 101; 
    }
    // Ahora si ordenamos los valoares generados
    qsort(valores, NUM_VALORES, sizeof(int), ordenarDeMayorAMenor);

    printf("Valores aleatorios (%d) ordenados de mayor a menor:\n", NUM_VALORES);
    for (int i = 0; i < NUM_VALORES; i++) {
        printf("%d ", valores[i]);
    }
    printf("\n");

    free(valores);

    return 0;
}
