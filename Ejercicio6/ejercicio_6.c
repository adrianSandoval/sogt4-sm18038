#include <stdio.h>
#include <stdlib.h>

int main() {
    int numDatos;
do {
        printf("Ingrese el número de datos (debe ser mayor que 0): ");
        scanf("%d", &numDatos);
        if (numDatos <= 0) {
            printf("Número no válido.\n");
        }
    } while (numDatos <= 0);

    float *datos = (float *)malloc(numDatos * sizeof(float));

    for (int i = 0; i < numDatos; i++) {
        printf("Ingrese el numero #%d: ", i + 1);
        scanf("%f", &datos[i]);
    }

    // Encontrar el máximo, el mínimo y calcular la media
    float valor_maximo = datos[0];
    float valor_minimo = datos[0];
    float suma = datos[0];

    for (int i = 1; i < numDatos; i++) {
        if (datos[i] > valor_maximo) {
            valor_maximo = datos[i];
        }
        if (datos[i] < valor_minimo) {
            valor_minimo = datos[i];
        }
        suma += datos[i];
    }

    float media_aritmetica = suma / numDatos;

    printf("\nValor máximo: %.2f\n", valor_maximo);
    printf("Valor mínimo: %.2f\n", valor_minimo);
    printf("Media Aritmetica: %.2f\n", media_aritmetica);

    free(datos);

    return 0;
}
