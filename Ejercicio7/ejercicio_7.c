#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double evaluarPolinomio(double *coeficientes, int grado, double x) {
    double resultado;
    for (int i = 0; i <= grado; i++) {
        resultado += coeficientes[i] * pow(x, i);
    }
    return resultado;
}

int main() {
    int grado;
    do {
        printf("Ingrese el grado del polinomio a evaluar: ");
        scanf("%d", &grado);
        if (grado <= 0) {
            printf("Número no válido.\n");
        }
    } while (grado <=0);
    
    double *coeficientes = (double *)malloc((grado + 1) * sizeof(double));

    // Pedir al usuario que ingrese los coeficientes del polinomio
    for (int i = grado; i >= 0; i--) {
        printf("Ingrese el coeficiente para x^%d: ", i);
        scanf("%lf", &coeficientes[i]);
    }
    double x;

    //punto (x) en el que se va a evaluar el polinomio
    printf("Ingrese el valor de x: ");
    scanf("%lf", &x);

    // Evaluar el polinomio y asignar a resultado
    double resultado = evaluarPolinomio(coeficientes, grado, x);

    printf("\nEl resultado de evaluar %.2f en el polinomio es: %.2f\n", x, resultado);

    printf("Polinomio evaluado: ");
    for (int i = grado; i >= 0; i--) {
        printf("%.2fx^%d", coeficientes[i], i);
        if (i > 0) {
            printf(" + ");
        }
    }
    printf("\n");

    free(coeficientes);

    return 0;
}
