#include <stdio.h>
#include <string.h>
#include <ctype.h>

char *pedir_texto() {
    char texto[1000]; 
    printf("Ingrese un texto para contar las vocales: ");
    fgets(texto, sizeof(texto), stdin);
    return strdup(texto); 
}

void contar_vocales(char *texto, int num[], int tamaño) {
    memset(num, 0, tamaño * sizeof(int)); 
    
    for (int i = 0; texto[i] != '\0'; i++) {
        char c = texto[i];
        switch (c) {
            case 'a':
            case 'A':
                num[0]++;
                break;
            case 'e':
            case 'E':
                num[1]++;
                break;
            case 'i':
            case 'I':
                num[2]++;
                break;
            case 'o':
            case 'O':
                num[3]++;
                break;
            case 'u':
            case 'U':
                num[4]++;
                break;
        }
    }
}

void imprimir(int num[]) {
    printf("\nVeces que se repiten las vocales:\n");
    printf("a: %d\n", num[0]);
    printf("e: %d\n", num[1]);
    printf("i: %d\n", num[2]);
    printf("o: %d\n", num[3]);
    printf("u: %d\n", num[4]);
}

int main() {
    char *texto;
    int num[5]; 
    texto = pedir_texto();
    contar_vocales(texto, num, sizeof(num) / sizeof(num[0]));
    imprimir(num);
    return 0;
}
