#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_PALABRAS 20
#define MAX_LONGITUD 50

int comparar(const void *a, const void *b) {
    return strcmp(*(const char **)a, *(const char **)b); 
}

int main() {
    char *palabras[MAX_PALABRAS];
    int num_palabras;
    do {
        printf("Ingrese el número de palabras (hasta %d): ", MAX_PALABRAS);
        scanf("%d", &num_palabras);
        
        if (num_palabras <= 0 || num_palabras > MAX_PALABRAS) {
            printf("Número de palabras no válido.\n");
        }
    } while (num_palabras <= 0 || num_palabras > MAX_PALABRAS);


    // Leer las palabras y almacenarlas en un arreglo
    for (int i = 0; i < num_palabras; i++) {
        palabras[i] = (char *)malloc(MAX_LONGITUD);
        printf("Ingrese la palabra #%d: ", i + 1);
        scanf("%s", palabras[i]);
    }

    qsort(palabras, num_palabras, sizeof(char *), comparar);


    printf("\nPalabras ordenadas en orden descendente:\n");
    for (int i = 0; i < num_palabras; i++) {
        printf("%s\n", palabras[i]);
        free(palabras[i]); 
    }

    return 0;
}
